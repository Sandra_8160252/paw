const jwt = require('jsonwebtoken');

module.exports = (req, res, next) => {
  try {
    const token = req.headers.authorization.split(" ")[1];
    jwt.verify(token, 'segredo_para_a_criacao_dos_tokens');
    next();
  } catch (err) {
    res.status(401).json({ message: "Auth failed!"});
  }
};
