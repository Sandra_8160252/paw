import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { Revenda } from '../revenda.model';
import { RevendasService } from '../revendas.service';
import { PageEvent } from '@angular/material';
import { AuthService } from 'src/app/auth/auth.service';


@Component({
  selector: 'app-revenda-list',
  templateUrl: './revenda-list.component.html',
  styleUrls: ['./revenda-list.component.css']
})
export class RevendaListComponent implements OnInit, OnDestroy {
  revendas: Revenda[] = [];
  isLoading = false;
  totalRevendas = 0;
  revendasPorPagina = 50;
  currentPage = 1;
  paginaSizeOptions = [5, 10, 50, 100];
  private revendasSub: Subscription;
  userIsAuthenticated = false;
  userId: string;
  private authListenerSubs: Subscription;
  currentTime: any;
  valor: number;
  isFuncionario = false;

  constructor(public revendasService: RevendasService, private authService: AuthService) {
    setInterval(() => {
      this.currentTime = new Date().getTime();
    }, 1);
    if (this.authService.getTipo() === 'funcionario') {
      this.isFuncionario = true;
    } else {
      this.isFuncionario = false;
    }
  }

  ngOnInit(): void {
    this.isLoading = true;
    this.revendasService.getRevendas(this.revendasPorPagina, this.currentPage);
    this.userId = this.authService.getUserID();
    this.revendasSub = this.revendasService.getRevendaAtualizadoListener()
      .subscribe((revendaData: { revendas: Revenda[], revendaCount: number }) => {
        this.isLoading = false;
        this.totalRevendas = revendaData.revendaCount;
        this.revendas = revendaData.revendas;
      });
    this.userIsAuthenticated = this.authService.getIsAuth();
    this.authListenerSubs = this.authService.getAuthStatusListener().subscribe(isAuthenticated => {
      this.userIsAuthenticated = isAuthenticated;
    });
  }

  getDate(data: Date) {
    return new Date(data).getTime();
  }

  getRestante(data: Date) {
    let restante = Math.round((new Date(data).getTime() + new Date().getTimezoneOffset() * 1000 * 60 - this.currentTime) / 1000);
    let restanteString = '';
    if (restante <= 0) {
      return 0;
    }
    if (restante > 86400) {
      restanteString += Math.floor(restante / 86400) + ' dia(s) e ';
      restante = Math.floor(restante % 86400);
    }
    if (restante > 3600) {
      restanteString += Math.floor(restante / 3600) + ' hora(s) e ';
      restante = Math.floor(restante % 3600);
    }
    if (restante > 60) {
      restanteString += Math.floor(restante / 60) + ' minuto(s) e ';
      restante = Math.floor(restante % 60);
    }
    restanteString += Math.floor(restante) + ' segundo(s)';
    return restanteString;
  }

  onChangedPage(pageData: PageEvent) {
    this.isLoading = true;
    this.currentPage = pageData.pageIndex + 1;
    this.revendasPorPagina = pageData.pageSize;
    this.revendasService.getRevendas(this.revendasPorPagina, this.currentPage);
  }

  valorIntroduzido(event: Event) {
    const introduzido = (event.target as HTMLInputElement).value;
    this.valor = Number(introduzido);
  }

  licitar(revenda: Revenda) {
    console.log(this.userId + ' licitou ' + this.valor + '€ na revenda ' + revenda.id);
    console.log();
    revenda.preco = this.valor;
    revenda.comprador = this.userId;
    this.revendasService.licitarUpdatecomprador(revenda);
  }

  onDelete(revendaId: string) {
    this.isLoading = true;
    this.revendasService.deleteRevenda(revendaId).subscribe(() => {
      this.revendasService.getRevendas(this.revendasPorPagina, this.currentPage);
    }, () => {
      this.isLoading = false;
    });
  }

  ngOnDestroy(): void {
    this.revendasSub.unsubscribe();
    this.authListenerSubs.unsubscribe();
  }
}
