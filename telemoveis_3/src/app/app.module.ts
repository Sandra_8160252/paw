import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import {
  MatInputModule,
  MatCardModule,
  MatButtonModule,
  MatToolbarModule,
  MatExpansionModule,
  MatProgressBarModule,
  MatPaginatorModule
} from '@angular/material';

import { AppComponent } from './app.component';
import { RevendaCreateComponent } from './revendas/revenda-create/revenda-create.component';
import { HeaderComponent } from './header/header.component';
import { RevendaListComponent } from './revendas/revenda-list/revenda-list.component';
import { AppRoutingModule } from './app-routing.module';
import { LoginComponent } from './auth/login/login.component';
import { SignUpComponent } from './auth/signup/signup.component';
import { AuthInterceptor } from './auth/auth-interceptor';
import { AdicionarTelemovelComponent } from "./Telemoveis/Adicionar/addTel.component";
import { ListarTelemoveisComponent } from "./Telemoveis/Listar/listTel.component";

@NgModule({
  declarations: [
    AppComponent,
    RevendaCreateComponent,
    HeaderComponent,
    RevendaListComponent,
    LoginComponent,
    SignUpComponent,
    AdicionarTelemovelComponent,
    ListarTelemoveisComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatCardModule,
    MatButtonModule,
    MatToolbarModule,
    MatExpansionModule,
    MatProgressBarModule,
    HttpClientModule,
    MatPaginatorModule,
    FormsModule
  ],
  providers: [{provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true}],
  bootstrap: [AppComponent]
})
export class AppModule { }
