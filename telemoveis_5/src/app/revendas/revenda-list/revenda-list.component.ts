import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { Revenda } from '../revenda.model';
import { RevendasService } from '../revendas.service';
import { PageEvent } from '@angular/material';
import { AuthService } from 'src/app/auth/auth.service';


@Component({
  selector: 'app-revenda-list',
  templateUrl: './revenda-list.component.html',
  styleUrls: ['./revenda-list.component.css']
})
export class RevendaListComponent implements OnInit, OnDestroy {
  revendas: Revenda[] = [];
  isLoading = false;
  totalRevendas = 0;
  revendasPorPagina = 10;
  currentPage = 1;
  paginaSizeOptions = [5, 10, 50, 100];
  private revendasSub: Subscription;
  userIsAuthenticated = false;
  userId: string;
  private authListenerSubs: Subscription;
  CurrentTime: any;
  valor: number;
  isFuncionario = false;

  constructor(public revendasService: RevendasService, private authService: AuthService) {
    setInterval(() => {
      this.CurrentTime = new Date().toTimeString().split(' ')[0];
    }, 1);
    if (this.authService.getTipo() === 'funcionario') {
      this.isFuncionario = true;
    } else {
      this.isFuncionario = false;
    }
  }

  ngOnInit(): void {
    this.isLoading = true;
    this.revendasService.getRevendas(this.revendasPorPagina, this.currentPage);
    this.userId = this.authService.getUserID();
    this.revendasSub = this.revendasService.getRevendaAtualizadoListener()
      .subscribe((revendaData: { revendas: Revenda[], revendaCount: number }) => {
        this.isLoading = false;
        this.totalRevendas = revendaData.revendaCount;
        this.revendas = revendaData.revendas;
      });
    this.userIsAuthenticated = this.authService.getIsAuth();
    this.authListenerSubs = this.authService.getAuthStatusListener().subscribe(isAuthenticated => {
      this.userIsAuthenticated = isAuthenticated;
    });
  }

  onChangedPage(pageData: PageEvent) {
    this.isLoading = true;
    this.currentPage = pageData.pageIndex + 1;
    this.revendasPorPagina = pageData.pageSize;
    this.revendasService.getRevendas(this.revendasPorPagina, this.currentPage);
  }

  valorIntroduzido(event: Event) {
    const introduzido = (event.target as HTMLInputElement).value;
    this.valor = Number(introduzido);
  }

  onDelete(revendaId: string) {
    this.isLoading = true;
    this.revendasService.deleteRevenda(revendaId).subscribe(() => {
      this.revendasService.getRevendas(this.revendasPorPagina, this.currentPage);
    }, () => {
      this.isLoading = false;
    });
  }

  ngOnDestroy(): void {
    this.revendasSub.unsubscribe();
    this.authListenerSubs.unsubscribe();
  }
}
