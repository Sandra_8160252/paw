const Revenda = require('../models/revenda');

exports.createRevendas = (req, res, next) => {
  const url = req.protocol + '://' + req.get("host");
  const revenda = new Revenda({
    title: req.body.title,
    descricao: req.body.descricao,
    imagePath: url + "/images/" + req.file.filename,
    criador: req.userData.userId
  });
  revenda.save().then(revendaCriado => {
    res.status(201).json({
      message: 'Revenda adicionada com sucesso',
      revenda: {
        ...revendaCriado,
        id: revendaCriado._id
      }
    });
  }).catch(error => {
    res.status(500).json({
      message: 'Criacao de revenda falhou!'
    });
  });
}

exports.updateRevenda = (req, res, next) => {
  let imagePath = req.body.imagePath;
  if (req.file) {
    const url = req.protocol + '://' + req.get("host");
    imagePath = url + "/images/" + req.file.filename;
  }
  const revenda = new Revenda({
    _id: req.body.id,
    title: req.body.title,
    descricao: req.body.descricao,
    imagePath: imagePath,
    criador: req.userData.userId
  });
  Revenda.updateOne({ _id: req.params.id, criador: req.userData.userId }, revenda).then(result => {
    if (result.n > 0) {
      res.status(200).json({ message: "Update com sucesso" });
    } else {
      res.status(401).json({ message: "Nao autorizado!" });
    }
  }).catch(error => {
    res.status(500).json({
      message: 'Update de revenda falhou!'
    });
  });
}

exports.getRevendas = (req, res, next) => {
  const pageSize = +req.query.pageSize;
  const currentPage = +req.query.page;
  const revendaQuery = Revenda.find();
  let fetchedRevendas;
  if (pageSize && currentPage) {
    revendaQuery.skip(pageSize * (currentPage - 1)).limit(pageSize);
  }
  revendaQuery.then(documents => {
    fetchedRevendas = documents;
    return Revenda.countDocuments();
  }).then(count => {
    res.status(200).json({
      message: 'Revendas buscadas com sucesso',
      revendas: fetchedRevendas,
      maxRevendas: count
    });
  }).catch(error => {
    res.status(500).json({
      message: 'Getting revendas falhou!'
    });
  });
}

exports.getRevenda = (req, res, next) => {
  Revenda.findById(req.params.id).then(revenda => {
    if (revenda) {
      res.status(200).json(revenda);
    } else {
      res.status(404).json({ message: 'Revenda nao encontrada!' });
    }
  }).catch(error => {
    res.status(500).json({
      message: 'Getting revenda falhou!'
    });
  });
}

exports.deleteRevenda = (req, res, next) => {
  Revenda.deleteOne({ _id: req.params.id, criador: req.userData.userId }).then(result => {
    if (result.n > 0) {
      res.status(200).json({ message: "Apagada com sucesso" });
    } else {
      res.status(401).json({ message: "Nao autorizado!" });
    }
  }).catch(error => {
    res.status(500).json({
      message: 'Apagar revenda falhou!'
    });
  });
}
