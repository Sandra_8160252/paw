const express = require('express');
const multer = require('multer');
const Telemovel = require('../models/telemovel');
const checkAuth = require('../middleware/check-auth');
const router = express.Router();

//Ver tipo da imagem
const MIME_TYPE_MAP = {
  'image/png': 'png',
  'image/jpeg': 'jpg',
  'image/jpg': 'jpg',
};

//Guardar a Imagem
const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    const isValid = MIME_TYPE_MAP[file.mimetype];
    let error = new Error('Invalid mime type');
    if (isValid) {
      error = null;
    }
    cb(error, 'backend/images');
  },
  filename: (req, file, cb) => {
    const name = file.originalname.toLowerCase().split(' ').join('-');
    const ext = MIME_TYPE_MAP[file.mimetype];
    cb(null, name + '-' + Date.now() + '.' + ext);
  }
});

router.post("", checkAuth, multer({ storage: storage }).single('image'), (req, res, next) => {
  const url = req.protocol + '://' + req.get("host");
  const telemovel = new Telemovel({
    modelo: req.body.modelo,
    marca: req.body.marca,
    descricao: req.body.descricao,
    preco: req.body.preco,
    data: req.body.data,
    imagePath: url + "/images/" + req.file.filename,
    criador: req.userData.userId
  });
  telemovel.save().then(telemovelCriado => {
    res.status(201).json({
      message: 'Telemovel adicionado com sucesso',
      telemovel: {
        ...telemovelCriado,
        id: telemovelCriado._id
      }
    });
  });
});

router.put('/:id', checkAuth, multer({ storage: storage }).single('image'), (req, res, next) => {
  let imagePath = req.body.imagePath;
  if (req.file) {
    const url = req.protocol + '://' + req.get("host");
    imagePath = url + "/images/" + req.file.filename;
  }
  const telemovel = new Telemovel({
    _id: req.body.id,
    modelo: req.body.modelo,
    marca: req.body.marca,
    descricao: req.body.descricao,
    preco: req.body.preco,
    data: req.body.data,
    imagePath: imagePath,
    criador: req.userData.userId
  });
  Telemovel.updateOne({ _id: req.params.id }, telemovel).then(result => {
    res.status(200).json({ message: "Update com sucesso" });
  });
});

router.get('', (req, res, next) => {
  const pageSize = +req.query.pageSize;
  const currentPage = +req.query.page;
  const telemovelQuery = Telemovel.find();
  let fetchedTelemoveis;
  if (pageSize && currentPage) {
    telemovelQuery.skip(pageSize * (currentPage - 1)).limit(pageSize);
  }
  telemovelQuery.then(documents => {
    fetchedTelemoveis = documents;
    return Telemovel.countDocuments();
  }).then(count => {
    res.status(200).json({
      message: 'Telemoveis buscados com sucesso',
      telemoveis: fetchedTelemoveis,
      maxTelemoveis: count
    });
  });
});

router.get('/:id', (req, res, next) => {
  Telemovel.findById(req.params.id).then(telemovel => {
    if (telemovel) {
      res.status(200).json(telemovel);
    } else {
      res.status(404).json({ message: 'Telemóvel não encontrado!' });
    }
  })
});

router.delete('/:id', checkAuth, (req, res, next) => {
  Telemovel.deleteOne({ _id: req.params.id }).then(result => {
    console.log(result);
    res.status(200).json({ message: 'Telemóvel apagado com sucesso' });
  });
});

module.exports = router;
