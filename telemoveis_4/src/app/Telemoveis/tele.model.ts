export interface Telemovel {
  id: string;
  title: string;
  brand: string;
  content: string;
  price: number;
  date: Date;
  image: File;
}
