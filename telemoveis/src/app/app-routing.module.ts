import { RevendaListComponent } from './revendas/revenda-list/revenda-list.component';
import { RevendaCreateComponent } from './revendas/revenda-create/revenda-create.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './auth/login/login.component';
import { SignUpComponent } from './auth/signup/signup.component';
import { AuthGuard } from './auth/auth.guard';
import { AdicionarTelemovelComponent } from "./Telemoveis/Adicionar/addTel.component";
import { ListarTelemoveisComponent } from "./Telemoveis/Listar/listTel.component";


const routes: Routes = [
  { path: '', component: RevendaListComponent },
  { path: 'create', component: RevendaCreateComponent, canActivate: [AuthGuard] },
  { path: 'edit/:revendaId', component: RevendaCreateComponent, canActivate: [AuthGuard] },
  { path: 'login', component: LoginComponent },
  { path: 'signup', component: SignUpComponent },
  { path: 'adicionarTelemoveis', component: AdicionarTelemovelComponent},
  { path: 'listarTelemoveis', component:ListarTelemoveisComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [AuthGuard]
})
export class AppRoutingModule { }
