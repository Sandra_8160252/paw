import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, ParamMap } from '@angular/router';

import { RevendasService } from '../revendas.service';
import { Revenda } from '../revenda.model';
import { mimeType } from './mime-type.validator';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/auth/auth.service';

@Component({
  selector: 'app-revenda-create',
  templateUrl: './revenda-create.component.html',
  styleUrls: ['./revenda-create.component.css']
})
export class RevendaCreateComponent implements OnInit, OnDestroy {
  revenda: Revenda;
  isLoading = false;
  form: FormGroup;
  imagePreview: string;
  private mode = 'create';
  private revendaId: string;
  private authStatusSub: Subscription;
  currentTime = new Date();
  aceite = false;
  aceiteS = '';
  isFuncionario = true;
  dataFimRevenda: Date;

  constructor(
    public revendasService: RevendasService,
    public route: ActivatedRoute,
    private authService: AuthService
  ) {
    if (this.authService.getTipo() === 'funcionario') {
      this.isFuncionario = true;
    } else {
      this.isFuncionario = false;
    }
  }

  ngOnInit() {
    this.authStatusSub = this.authService
      .getAuthStatusListener()
      .subscribe(authStatus => {
        this.isLoading = false;
      });
    this.form = new FormGroup({
      nome: new FormControl(null, {
        validators: [Validators.required, Validators.minLength(1), Validators.maxLength(100)]
      }),
      marca: new FormControl(null, {
        validators: [Validators.required, Validators.minLength(1), Validators.maxLength(100)]
      }),
      modelo: new FormControl(null, {
        validators: [Validators.required, Validators.minLength(1), Validators.maxLength(100)]
      }),
      descricao: new FormControl(null, {
        validators: [Validators.required, Validators.minLength(1), Validators.maxLength(1000)]
      }),
      image: new FormControl(null, {
        validators: [Validators.required], asyncValidators: [mimeType]
      }),
      preco: new FormControl(0, {
        validators: [Validators.required, Validators.min(0), Validators.max(10000)]
      }),
      tempo: new FormControl(0, {
        validators: [Validators.required, Validators.min(0), Validators.max(1000)]
      }),
      aceite: new FormControl('nao', {
        validators: [Validators.required]
      })
    });
    this.route.paramMap.subscribe((paramMap: ParamMap) => {
      if (paramMap.has('revendaId')) {
        this.mode = 'edit';
        this.revendaId = paramMap.get('revendaId');
        this.isLoading = true;
        this.revendasService.getRevenda(this.revendaId).subscribe(revendaData => {
          this.isLoading = false;

          this.revenda = {
            id: revendaData._id,
            nome: revendaData.nome,
            marca: revendaData.marca,
            modelo: revendaData.modelo,
            descricao: revendaData.descricao,
            imagePath: revendaData.imagePath,
            criador: revendaData.criador,
            preco: revendaData.preco,
            data: revendaData.data,
            tempo: revendaData.tempo,
            aceite: revendaData.aceite,
            comprador: revendaData.comprador
          };
          if (this.revenda.aceite) {
            this.aceiteS = 'sim';
          } else {
            this.aceiteS = 'nao';
          }
          this.form.setValue({
            nome: this.revenda.nome,
            marca: this.revenda.marca,
            modelo: this.revenda.modelo,
            descricao: this.revenda.descricao,
            image: this.revenda.imagePath,
            preco: this.revenda.preco,
            tempo: this.revenda.tempo,
            aceite: 'nao'
          });
        });
      } else {
        this.mode = 'create';
        this.revendaId = null;
      }
    });
  }

  onImagePicked(event: Event) {
    const file = (event.target as HTMLInputElement).files[0];
    this.form.patchValue({ image: file });
    this.form.get('image').updateValueAndValidity();
    const reader = new FileReader();
    reader.onload = () => {
      this.imagePreview = reader.result as string;
    };
    reader.readAsDataURL(file);
  }

  onSaveRevenda() {
    if (this.form.invalid) {
      return;
    }
    this.isLoading = true;
    if (this.form.value.aceite === 'sim') {
      this.aceite = true;
    } else {
      this.aceite = false;
    }

    this.dataFimRevenda = new Date(
      new Date().getTime() -
      new Date().getTimezoneOffset() * 1000 * 60 +
      this.form.value.tempo * 1000 * 60 * 60);

    if (this.mode === 'create') {
      this.revendasService
        .addRevenda(
          this.form.value.nome,
          this.form.value.marca,
          this.form.value.modelo,
          this.form.value.descricao,
          this.form.value.image,
          this.form.value.preco,
          this.dataFimRevenda,
          this.form.value.tempo,
          this.aceite
        );
    } else {
      this.revendasService
        .updateRevenda(
          this.revendaId,
          this.form.value.nome,
          this.form.value.marca,
          this.form.value.modelo,
          this.form.value.descricao,
          this.form.value.image,
          this.revenda.criador,
          this.form.value.preco,
          this.dataFimRevenda,
          this.form.value.tempo,
          this.aceite,
          'novo2'
        );
    }
    this.form.reset();
  }

  ngOnDestroy() {
    this.authStatusSub.unsubscribe();
  }

}
