const express = require('express');
const userControler = require('../controllers/user');
const router = express.Router();

router.post('/signup', userControler.createUser);

router.post('/login', userControler.loginUSer);

module.exports = router;
