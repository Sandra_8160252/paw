const path = require('path');
const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

const revendasRoutes = require('./routes/revenda');
const userRoutes = require('./routes/user');
const telemovelRoutes = require("./routes/telemovel");

const app = express();

mongoose.connect('mongodb+srv://BigSnorlax97:tenhoumapasswordfraca@cluster0-b9muq.mongodb.net/telemoveis?retryWrites=true&w=majority', { useCreateIndex: true, useNewUrlParser: true })
  .then(() => {
    console.log('connected to database');
  })
  .catch(() => {
    console.log('connecao falhou');
  });

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use("/images", express.static(path.join("backend/images")));

app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PATCH, PUT, DELETE, OPTIONS');
  next();
});

app.use('/api/revendas/', revendasRoutes);
app.use('/api/user/', userRoutes);
app.use("/api/telemoveis/", telemovelRoutes);

module.exports = app;
