import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-tel-list',
  templateUrl: './listTel.component.html',
  styleUrls: ['./listTel.component.css']
})
export class ListarTelemoveisComponent {
  @Input() telemoveis = [];
}
