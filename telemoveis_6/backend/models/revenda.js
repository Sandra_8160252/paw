const mongoose = require('mongoose');

const revendaSchema = mongoose.Schema({
  nome: { type: String, required: true },
  marca: { type: String, required: true },
  modelo: { type: String, required: true },
  descricao: { type: String, required: true },
  imagePath: { type: String, required: true },
  criador: { type: mongoose.Schema.Types.ObjectId, ref: "User", required: true },
  preco: { type: Number, required: true },
  data: { type: Date, require: true },
  tempo: { type: Number, require: true },
  aceite: { type: Boolean, require: true },
  comprador: { type: String }
});

module.exports = mongoose.model('Revenda', revendaSchema);
