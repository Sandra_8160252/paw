export interface Revenda {
  id: string;
  title: string;
  descricao: string;
  imagePath: string;
}
