const express = require('express');
const multer = require('multer');
const Revenda = require('../models/revenda');
const checkAuth = require('../middleware/check-auth');
const router = express.Router();

const MIME_TYPE_MAP = {
  'image/png': 'png',
  'image/jpeg': 'jpg',
  'image/jpg': 'jpg',
};

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    const isValid = MIME_TYPE_MAP[file.mimetype];
    let error = new Error('Invalid mime type');
    if (isValid) {
      error = null;
    }
    cb(error, 'backend/images');
  },
  filename: (req, file, cb) => {
    const name = file.originalname.toLowerCase().split(' ').join('-');
    const ext = MIME_TYPE_MAP[file.mimetype];
    cb(null, name + '-' + Date.now() + '.' + ext);
  }
});

router.post("", checkAuth, multer({ storage: storage }).single('image'), (req, res, next) => {
  const url = req.protocol + '://' + req.get("host");
  const revenda = new Revenda({
    title: req.body.title,
    descricao: req.body.descricao,
    imagePath: url + "/images/" + req.file.filename,
    criador: req.userData.userId
  });
  revenda.save().then(revendaCriado => {
    res.status(201).json({
      message: 'Revenda adicionada com sucesso',
      revenda: {
        ...revendaCriado,
        id: revendaCriado._id
      }
    });
  });
});

router.put('/:id', checkAuth, multer({ storage: storage }).single('image'), (req, res, next) => {
  let imagePath = req.body.imagePath;
  if (req.file) {
    const url = req.protocol + '://' + req.get("host");
    imagePath = url + "/images/" + req.file.filename;
  }
  const revenda = new Revenda({
    _id: req.body.id,
    title: req.body.title,
    descricao: req.body.descricao,
    imagePath: imagePath
  });
  Revenda.updateOne({ _id: req.params.id }, revenda).then(result => {
    res.status(200).json({ message: "Update com sucesso" });
  });
});

router.get('', (req, res, next) => {
  const pageSize = +req.query.pageSize;
  const currentPage = +req.query.page;
  const revendaQuery = Revenda.find();
  let fetchedRevendas;
  if (pageSize && currentPage) {
    revendaQuery.skip(pageSize * (currentPage - 1)).limit(pageSize);
  }
  revendaQuery.then(documents => {
    fetchedRevendas = documents;
    return Revenda.countDocuments();
  }).then(count => {
    res.status(200).json({
      message: 'Revendas buscadas com sucesso',
      revendas: fetchedRevendas,
      maxRevendas: count
    });
  });
});

router.get('/:id', (req, res, next) => {
  Revenda.findById(req.params.id).then(revenda => {
    if (revenda) {
      res.status(200).json(revenda);
    } else {
      res.status(404).json({ message: 'Revenda nao encontrada!' });
    }
  })
});

router.delete('/:id', checkAuth, (req, res, next) => {
  Revenda.deleteOne({ _id: req.params.id }).then(result => {
    console.log(result);
    res.status(200).json({ message: 'Revenda apagada com sucesso' });
  });
});

module.exports = router;
