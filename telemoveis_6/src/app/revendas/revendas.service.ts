import { Revenda } from './revenda.model';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';

const BACKEND_URL = 'http://localhost:3000/api/revendas/';

@Injectable({ providedIn: 'root' })
export class RevendasService {
  private revendas: Revenda[] = [];
  private revendasAtualizados = new Subject<{ revendas: Revenda[], revendaCount: number }>();

  constructor(private http: HttpClient, private router: Router) { }

  getRevendas(revendasPorPagina: number, currentPage: number) {
    const queryParams = `?pageSize=${revendasPorPagina}&page=${currentPage}`;
    this.http.get<{ message: string, revendas: any, maxRevendas: number }>
      (BACKEND_URL + queryParams)
      .pipe(map((revendaData) => {
        return {
          revendas: revendaData.revendas.map(revenda => {
            return {
              id: revenda._id,
              nome: revenda.nome,
              marca: revenda.marca,
              modelo: revenda.modelo,
              descricao: revenda.descricao,
              imagePath: revenda.imagePath,
              criador: revenda.criador,
              preco: revenda.preco,
              data: revenda.data,
              tempo: revenda.tempo,
              aceite: revenda.aceite,
              comprador: revenda.comprador
            };
          }), maxRevendas: revendaData.maxRevendas
        };
      }))
      .subscribe((revendasTransformadosData) => {
        console.log(revendasTransformadosData);
        this.revendas = revendasTransformadosData.revendas;
        this.revendasAtualizados.next({ revendas: [...this.revendas], revendaCount: revendasTransformadosData.maxRevendas });
      });
  }

  getRevendaAtualizadoListener() {
    return this.revendasAtualizados.asObservable();
  }

  getRevenda(id: string) {
    return this.http.get<{
      _id: string,
      nome: string,
      marca: string,
      modelo: string,
      descricao: string,
      imagePath: string,
      criador: string,
      preco: number,
      data: Date,
      tempo: number,
      aceite: boolean,
      comprador: string
    }>(BACKEND_URL + id);
  }

  addRevenda(
    nome: string,
    marca: string,
    modelo: string,
    descricao: string,
    image: File,
    preco: number,
    data: Date,
    tempo: number,
    aceite: boolean
  ) {
    const revendaData = new FormData();
    revendaData.append('nome', nome);
    revendaData.append('marca', marca);
    revendaData.append('modelo', modelo);
    revendaData.append('descricao', descricao);
    revendaData.append('image', image, nome);
    revendaData.append('preco', String(preco));
    revendaData.append('data', String(data));
    revendaData.append('tempo', String(tempo));
    revendaData.append('aceite', String(aceite));
    this.http
      .post<{ message: string, revenda: Revenda }>(BACKEND_URL, revendaData)
      .subscribe((responseData) => {
        this.router.navigate(['/']);
      });
  }

  updateRevenda(
    id: string,
    nome: string,
    marca: string,
    modelo: string,
    descricao: string,
    image: File | string,
    criador: string,
    preco: number,
    data: Date,
    tempo: number,
    aceite: boolean,
    comprador: string
  ) {
    let revendaData: Revenda | FormData;
    if (typeof (image) === 'object') {
      revendaData = new FormData();
      revendaData.append('id', id);
      revendaData.append('nome', nome);
      revendaData.append('marca', marca);
      revendaData.append('modelo', modelo);
      revendaData.append('descricao', descricao);
      revendaData.append('image', image);
      revendaData.append('preco', String(preco));
      revendaData.append('data', String(data));
      revendaData.append('tempo', String(tempo));
      revendaData.append('aceite', String(aceite));
      revendaData.append('comprador', String(comprador));
    } else {
      revendaData = { id, nome, marca, modelo, descricao, imagePath: image, criador, preco, data, tempo, aceite, comprador };
    }
    this.http
      .put(BACKEND_URL + id, revendaData)
      .subscribe(response => {
        this.router.navigate(['/']);
      });
  }

  licitarUpdatecomprador(revenda: Revenda) {
    this.http
      .put(BACKEND_URL + revenda.id, revenda)
      .subscribe(response => {
        this.router.navigate(['/']);
      });
  }

  deleteRevenda(revendaId: string) {
    return this.http
      .delete(BACKEND_URL + revendaId);
  }
}
