const express = require('express');
const checkAuth = require('../middleware/check-auth');
const extractFile = require('../middleware/file');
const router = express.Router();
const revendaController = require('../controllers/revenda')


router.post("", checkAuth, extractFile, revendaController.createRevendas);

router.put('/:id', checkAuth, extractFile, revendaController.updateRevenda);

router.get('/:id', revendaController.getRevenda);

router.get('', revendaController.getRevendas);

router.delete('/:id', checkAuth, revendaController.deleteRevenda);

module.exports = router;
