const mongoose = require('mongoose');

const revendaSchema = mongoose.Schema({
  title: { type: String, required: true },
  descricao: { type: String, required: true },
  imagePath: { type: String, required: true },
  criador: { type: mongoose.Schema.Types.ObjectId, ref: "User", required: true }
});

/*
const revendaSchema = mongoose.Schema({
  nome: { type: String, required: true },
  modelo: { type: String, required: true },
  marca: { type: String, required: true },
  descricao: { type: String, required: true },
  imagePath: { type: String, required: true },
  criador: { type: mongoose.Schema.Types.ObjectId, required: true },
  preco: { type: Number, required: true },
  dataFim: { type: Date, required: true}
});
*/

module.exports = mongoose.model('Revenda', revendaSchema);
