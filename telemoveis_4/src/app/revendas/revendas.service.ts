import { Revenda } from './revenda.model';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';

const BACKEND_URL = 'http://localhost:3000/api/revendas/';

@Injectable({ providedIn: 'root' })
export class RevendasService {
  private revendas: Revenda[] = [];
  private revendasAtualizados = new Subject<{ revendas: Revenda[], revendaCount: number }>();

  constructor(private http: HttpClient, private router: Router) { }

  getRevendas(revendasPorPagina: number, currentPage: number) {
    const queryParams = `?pageSize=${revendasPorPagina}&page=${currentPage}`;
    this.http.get<{ message: string, revendas: any, maxRevendas: number }>
      (BACKEND_URL + queryParams)
      .pipe(map((revendaData) => {
        return {
          revendas: revendaData.revendas.map(revenda => {
            return {
              title: revenda.title,
              descricao: revenda.descricao,
              id: revenda._id,
              imagePath: revenda.imagePath,
              criador: revenda.criador
            };
          }), maxRevendas: revendaData.maxRevendas
        };
      }))
      .subscribe((revendasTransformadosData) => {
        console.log(revendasTransformadosData);
        this.revendas = revendasTransformadosData.revendas;
        this.revendasAtualizados.next({ revendas: [...this.revendas], revendaCount: revendasTransformadosData.maxRevendas });
      });
  }

  getRevendaAtualizadoListener() {
    return this.revendasAtualizados.asObservable();
  }

  getRevenda(id: string) {
    return this.http.get<{
      _id: string,
      title: string,
      descricao: string,
      imagePath: string,
      criador: string
    }>(BACKEND_URL + id);
  }

  addRevenda(title: string, descricao: string, image: File) {
    const revendaData = new FormData();
    revendaData.append('title', title);
    revendaData.append('descricao', descricao);
    revendaData.append('image', image, title);
    this.http
      .post<{ message: string, revenda: Revenda }>(BACKEND_URL, revendaData)
      .subscribe((responseData) => {
        this.router.navigate(['/']);
      });
  }

  updateRevenda(id: string, title: string, descricao: string, image: File | string) {
    let revendaData: Revenda | FormData;
    if (typeof (image) === 'object') {
      revendaData = new FormData();
      revendaData.append('id', id);
      revendaData.append('title', title);
      revendaData.append('descricao', descricao);
      revendaData.append('image', image);
    } else {
      revendaData = { id, title, descricao, imagePath: image, criador: null };
    }
    this.http
      .put(BACKEND_URL + id, revendaData)
      .subscribe(response => {
        this.router.navigate(['/']);
      });
  }

  deleteRevenda(revendaId: string) {
    return this.http
      .delete(BACKEND_URL + revendaId);
  }
}
