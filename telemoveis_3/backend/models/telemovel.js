const mongoose = require('mongoose');

const telemovelSchema = mongoose.Schema({
  modelo: { type: String, required: true },
  marca: { type: String, required: true },
  descricao: { type: String, required: true },
  criador: { type: mongoose.Schema.Types.ObjectId, required: true },
  preco: { type: Number, required: true },
  dataFim: { type: Date, required: true},
  imagePath: { type: String, required: true },
});


module.exports = mongoose.model('Telemovel', telemovelSchema);
