import { Telemovel } from "./tele.model";
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Subject } from "rxjs";
import { map } from "rxjs/operators";
import { Router } from "@angular/router";

@Injectable({ providedIn: "root" })
export class TelemoveisService {
  private telemoveis: Telemovel[] = [];
  private telemoveisAtualizados = new Subject<{
    telemoveis: Telemovel[];
    telemovelCount: number;
  }>();

  constructor(private http: HttpClient, private router: Router) {}

  getTelemoveis(telemoveisPorPagina: number, currentPage: number) {
    const queryParams = `?pageSize=${telemoveisPorPagina}&page=${currentPage}`;
    this.http
      .get<{ message: string; telemoveis: any; maxTelemoveis: number }>(
        "http://localhost:3000/api/telemoveis" + queryParams
      )
      .pipe(
        map(telemovelData => {
          return {
            telemoveis: telemovelData.telemoveis.map(telemovel => {
              return {
                title: telemovel.title,
                content: telemovel.content,
                id: telemovel._id,
                brand: telemovel.brand,
                price: telemovel.price,
                date: telemovel.date,
                image: telemovel.image
              };
            }),
            maxTelemoveis: telemovelData.maxTelemoveis
          };
        })
      )
      .subscribe(telemoveisTransformadosData => {
        this.telemoveis = telemoveisTransformadosData.telemoveis;
        this.telemoveisAtualizados.next({
          telemoveis: [...this.telemoveis],
          telemovelCount: telemoveisTransformadosData.maxTelemoveis
        });
      });
  }

  getTelemovelAtualizadoListener() {
    return this.telemoveisAtualizados.asObservable();
  }

  getTelemovel(id: string) {
    return this.http.get<{
      _id: string;
      title: string;
      content: string;
      brand: string;
      price: number;
      date: Date;
      image: File;
    }>("http://localhost:3000/api/telemoveis/" + id);
  }

  addTelemovel(
    title: string,
    content: string,
    brand: string,
    price: number,
    date: Date,
    image: File
  ) {
    const telemovelData = new FormData();
    telemovelData.append("title", title);
    telemovelData.append("content", content);
    telemovelData.append("brand", brand);
    telemovelData.append("price", price.toString());
    telemovelData.append("date", date.toString());
    telemovelData.append("image", image);

    this.http
      .post<{ message: string; telemovel: Telemovel }>(
        "http://localhost:3000/api/telemoveis",
        telemovelData
      )
      .subscribe(responseData => {
        this.router.navigate(["/"]);
      });
  }

  updateTelemovel(
    id: string,
    title: string,
    content: string,
    brand: string,
    price: number,
    date: Date,
    image: File
  ) {
    let telemovelData: Telemovel | FormData;
    telemovelData = new FormData();
    telemovelData = { id, title, content, brand, price, date, image };
    this.http
      .put("http://localhost:3000/api/telemoveis/" + id, telemovelData)
      .subscribe(response => {
        this.router.navigate(["/"]);
      });
  }

  deleteTelemovel(telemovelId: string) {
    return this.http.delete(
      "http://localhost:3000/api/telemoveis/" + telemovelId
    );
  }
}
