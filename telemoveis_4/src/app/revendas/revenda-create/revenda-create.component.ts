import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, ParamMap } from '@angular/router';

import { RevendasService } from '../revendas.service';
import { Revenda } from '../revenda.model';
import { mimeType } from './mime-type.validator';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/auth/auth.service';

@Component({
  selector: 'app-revenda-create',
  templateUrl: './revenda-create.component.html',
  styleUrls: ['./revenda-create.component.css']
})
export class RevendaCreateComponent implements OnInit, OnDestroy {
  enteredTitle = '';
  enteredDescricao = '';
  revenda: Revenda;
  isLoading = false;
  form: FormGroup;
  imagePreview: string;
  private mode = 'create';
  private revendaId: string;
  private authStatusSub: Subscription;

  constructor(
    public revendasService: RevendasService,
    public route: ActivatedRoute,
    private authService: AuthService
  ) { }

  ngOnInit() {
    this.authStatusSub = this.authService
      .getAuthStatusListener()
      .subscribe(authStatus => {
        this.isLoading = false;
      });
    this.form = new FormGroup({
      title: new FormControl(null, {
        validators: [Validators.required, Validators.minLength(3)]
      }),
      descricao: new FormControl(null, { validators: [Validators.required] }),
      image: new FormControl(null, {
        validators: [Validators.required],
        asyncValidators: [mimeType]
      })
    });
    this.route.paramMap.subscribe((paramMap: ParamMap) => {
      if (paramMap.has('revendaId')) {
        this.mode = 'edit';
        this.revendaId = paramMap.get('revendaId');
        this.isLoading = true;
        this.revendasService.getRevenda(this.revendaId).subscribe(revendaData => {
          this.isLoading = false;
          this.revenda = {
            id: revendaData._id,
            title: revendaData.title,
            descricao: revendaData.descricao,
            imagePath: revendaData.imagePath,
            criador: revendaData.criador
          };
          this.form.setValue({ title: this.revenda.title, descricao: this.revenda.descricao, image: this.revenda.imagePath });
        });
      } else {
        this.mode = 'create';
        this.revendaId = null;
      }
    });
  }

  onImagePicked(event: Event) {
    const file = (event.target as HTMLInputElement).files[0];
    this.form.patchValue({ image: file });
    this.form.get('image').updateValueAndValidity();
    const reader = new FileReader();
    reader.onload = () => {
      this.imagePreview = reader.result as string;
    };
    reader.readAsDataURL(file);
  }

  onSaveRevenda() {
    if (this.form.invalid) {
      return;
    }
    this.isLoading = true;
    if (this.mode === 'create') {
      this.revendasService.addRevenda(this.form.value.title, this.form.value.descricao, this.form.value.image);
    } else {
      this.revendasService.updateRevenda(this.revendaId, this.form.value.title, this.form.value.descricao, this.form.value.image);
    }
    this.form.reset();
  }

  ngOnDestroy() {
    this.authStatusSub.unsubscribe();
  }

}
