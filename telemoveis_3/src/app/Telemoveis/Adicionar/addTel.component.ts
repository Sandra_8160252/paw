import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { ActivatedRoute, ParamMap } from "@angular/router";

import { TelemoveisService } from "../tele.service";
import { Telemovel } from "../tele.model";

@Component({
  templateUrl: "./addTel.component.html",
  styleUrls: ["./addTel.component.css"]
})

export class AdicionarTelemovelComponent implements OnInit {
  enteredTitle = "";
  enteredBrand = "";
  enteredContent = "";
  enteredPrice: number;
  enteredDate: Date;
  enteredImage: File;
  telemovel: Telemovel;
  isLoading = false;
  form: FormGroup;
  imagePreview: string;
  private mode = "create";
  private telemovelId: string;

  constructor(
    public telemoveisService: TelemoveisService,
    public route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.form = new FormGroup({
      title: new FormControl(null, {
        validators: [Validators.required, Validators.minLength(3)]
      }),
      content: new FormControl(null, { validators: [Validators.required] }),
      brand: new FormControl(null, {
        validators: [Validators.required, Validators.minLength(3)]
      }),
      price: new FormControl(null, {
        validators: [Validators.required, Validators.min(0)]
      }),
      date: new FormControl(null, {
        validators: [Validators.required]
      })
    });
    this.route.paramMap.subscribe((paramMap: ParamMap) => {
      if (paramMap.has("telemovelId")) {
        this.mode = "edit";
        this.telemovelId = paramMap.get("telemovelId");
        this.isLoading = true;
        this.telemoveisService
          .getTelemovel(this.telemovelId)
          .subscribe(telemovelData => {
            this.isLoading = false;
            this.telemovel = {
              id: telemovelData._id,
              title: telemovelData.title,
              content: telemovelData.content,
              brand: telemovelData.brand,
              price: telemovelData.price,
              date: telemovelData.date,
              image: null
            };
            this.form.setValue({
              title: this.telemovel.title,
              content: this.telemovel.content,
              brand: this.telemovel.brand,
              price: this.telemovel.price,
              date: this.telemovel.date
            });
          });
      } else {
        this.mode = "create";
        this.telemovelId = null;
      }
    });
  }

  onImagePicked(event: Event) {
    const file = (event.target as HTMLInputElement).files[0];
    this.form.patchValue({ image: file });
    this.form.get("image").updateValueAndValidity();
    const reader = new FileReader();
    reader.onload = () => {
      this.imagePreview = reader.result as string;
    };
    reader.readAsDataURL(file);
  }

  onSaveTelemovel() {
    if (this.form.invalid) {
      console.log(this.form.value.title,
        this.form.value.content,
        this.form.value.brand,
        this.form.value.price,
        this.form.value.date,
        this.form.invalid
        );
      return;
    }
    this.isLoading = true;
    if (this.mode === "create") {
      this.telemoveisService.addTelemovel(
        this.form.value.title,
        this.form.value.content,
        this.form.value.brand,
        this.form.value.price,
        this.form.value.date,
         null
      );
    } else {
      this.telemoveisService.updateTelemovel(
        this.telemovelId,
        this.form.value.title,
        this.form.value.content,
        this.form.value.brand,
        this.form.value.price,
        this.form.value.date,
        null
      );
    }
    this.form.reset();
  }
}
