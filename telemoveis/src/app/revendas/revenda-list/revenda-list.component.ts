import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { Revenda } from '../revenda.model';
import { RevendasService } from '../revendas.service';
import { PageEvent } from '@angular/material';
import { AuthService } from 'src/app/auth/auth.service';


@Component({
  selector: 'app-revenda-list',
  templateUrl: './revenda-list.component.html',
  styleUrls: ['./revenda-list.component.css']
})
export class RevendaListComponent implements OnInit, OnDestroy {
  revendas: Revenda[] = [];
  isLoading = false;
  totalRevendas = 0;
  revendasPorPagina = 10;
  currentPage = 1;
  paginaSizeOptions = [5, 10, 50, 100];
  private revendasSub: Subscription;
  userIsAuthenticated = false;
  private authListenerSubs: Subscription;

  constructor(public revendasService: RevendasService, private authService: AuthService) { }

  ngOnInit(): void {
    this.isLoading = true;
    this.revendasService.getRevendas(this.revendasPorPagina, this.currentPage);
    this.revendasSub = this.revendasService.getRevendaAtualizadoListener()
      .subscribe((revendaData: { revendas: Revenda[], revendaCount: number }) => {
        this.isLoading = false;
        this.totalRevendas = revendaData.revendaCount;
        this.revendas = revendaData.revendas;
      });
    this.userIsAuthenticated = this.authService.getIsAuth();
    this.authListenerSubs = this.authService.getAuthStatusListener().subscribe(isAuthenticated => {
      this.userIsAuthenticated = isAuthenticated;
    });
  }

  onChangedPage(pageData: PageEvent) {
    this.isLoading = true;
    this.currentPage = pageData.pageIndex + 1;
    this.revendasPorPagina = pageData.pageSize;
    this.revendasService.getRevendas(this.revendasPorPagina, this.currentPage);
  }

  onDelete(revendaId: string) {
    this.isLoading = true;
    this.revendasService.deleteRevenda(revendaId).subscribe(() => {
      this.revendasService.getRevendas(this.revendasPorPagina, this.currentPage);
    });
  }

  ngOnDestroy(): void {
    this.revendasSub.unsubscribe();
    this.authListenerSubs.unsubscribe();
  }
}
