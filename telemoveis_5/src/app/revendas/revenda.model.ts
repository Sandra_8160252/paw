export interface Revenda {
  id: string;
  nome: string;
  marca: string;
  modelo: string;
  descricao: string;
  imagePath: string;
  criador: string;
  preco: number;
  data: Date;
  tempo: number;
  aceite: boolean;
}
